# Key Repositories

## [RF Blocks Hardware Control](https://gitlab.com/dyadic/rfblocks)

Code for the [rfblocks Python
framework](https://rfblocks.org/rfblocks-sw/index.html) containing classes
for control of hardware modules. These classes model the internal hardware
registers of the modules and allow for configuration and command.

## RF Blocks Embedded Command Processing

The ECApp firmware is designed to provide simple, low level access to the
underlying controller hardware.

### [For the Raspberry Pi pico (Zephyr RTOS)](https://gitlab.com/dyadic/ecapp/-/tree/master/zephyr/ecapp?ref_type=heads)

### [For the Raspberry Pi Pico (Bare Metal)](https://gitlab.com/dyadic/ecapp/-/tree/master/pico/apps/cmdproc?ref_type=heads)

### [For the Atmel Atmega32u2](https://gitlab.com/dyadic/ecapp/-/tree/usbcmd/atmega32u2/src?ref_type=heads)

## [RF Blocks LTC5582/LT5537 Power Detector Control](https://gitlab.com/dyadic/ecapp/-/tree/spiadc/attiny45?ref_type=heads)

Attiny45 firmware for SPI control of [LTC5582/LT5537 power
detector](https://rfblocks.org/boards/LTC5582-Power-Detector.html#firmware)
ADC. As a bonus, detector calibration data is stored in the Attiny EEPROM.

## [Zero Copy Linux DMA Driver](https://gitlab.com/dyadic/xilinx-axidma)

A Xilinx AXI DMA Linux kernel driver and user space library together with
Python bindings. Used with the Zynq 7000 and Red Pitaya board to implement
[a dual ADC/DAC for signal acquisition](https://rfblocks.org/refdesigns/DualDacAdc/Dual-DAC-ADC.html).

## [RF Blocks Hardware Modules](https://gitlab.com/dyadic-groups/rfblocks-hardware-modules)

RF Blocks modules provide specific, discrete RF signal processing functions such as gain
blocks, frequency synthesizers, mixers, RF switches, attenuators. RF Blocks modules are
open hardware and all design files are available under the [CERN-OHL-S v2
license](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2).

## [RF Blocks Reference Designs](https://gitlab.com/dyadic-groups/rfblocks-reference-designs)

Designs which make use of RF Blocks hardware modules. These currently include
RF signal generators, a broadband RF power meter, a versatile clock generator
and wideband frequency converters.


